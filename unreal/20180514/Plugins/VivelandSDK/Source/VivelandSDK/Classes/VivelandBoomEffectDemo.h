// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "VivelandBoomEffect.h"

#include "VivelandBoomEffectDemo.generated.h"


UCLASS( ClassGroup=(Viveland), meta=(BlueprintSpawnableComponent) )
class VIVELANDSDK_API UVivelandBoomEffectDemo : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
	UVivelandBoomEffectDemo();

    // Called when the game starts
    void BeginPlay() override;

    void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

    // Called every frame
    void TickComponent(
            float DeltaTime,
            ELevelTick TickType,
            FActorComponentTickFunction* ThisTickFunction
    ) override;

};
