// Fill out your copyright notice in the Description page of Project Settings.

#include "VivelandBoomEffect.h"
#include "VivelandSDKPrivatePCH.h"
#include "VivelandSDK.h"
#include "vivelandeffect_api.h"

bool UVivelandBoomEffect::init(int id)
{
	if (FVivelandSDKModule::IsAvailable())
	{
		UE_LOG(VivelandSDK, Log, TEXT("[UVivelandBoomEffect][init]"));
		return VivelandBoomEffect()->init(id);
	}
	return false;
}

void UVivelandBoomEffect::deinit()
{
	if (FVivelandSDKModule::IsAvailable())
	{
		UE_LOG(VivelandSDK, Log, TEXT("[UVivelandBoomEffect][deinit]"));
		VivelandBoomEffect()->deinit();
	}
}

int UVivelandBoomEffect::getPorts()
{
	if (FVivelandSDKModule::IsAvailable())
	{
		UE_LOG(VivelandSDK, Log, TEXT("[UVivelandBoomEffect][init]"));
		return VivelandBoomEffect()->getPorts();
	}
	return 0;
}


void UVivelandBoomEffect::play(int index, int port)
{
	if (FVivelandSDKModule::IsAvailable())
	{
		UE_LOG(VivelandSDK, Log, TEXT("[UVivelandBoomEffect][play]"));
		VivelandBoomEffect()->play(index, port);
	}
}



void UVivelandBoomEffect::setVol(int vol, int port)
{
	if (FVivelandSDKModule::IsAvailable())
	{
		UE_LOG(VivelandSDK, Log, TEXT("[UVivelandBoomEffect][setVol]"));
		VivelandBoomEffect()->setVol(vol, port);
	}
}

void UVivelandBoomEffect::stop(int port)
{
	if (FVivelandSDKModule::IsAvailable())
	{
		UE_LOG(VivelandSDK, Log, TEXT("[UVivelandBoomEffect][stop]"));
		VivelandBoomEffect()->stop(port);
	}
}
