// Fill out your copyright notice in the Description page of Project Settings.

#include "VivelandBoomEffectDemo.h"
#include "VivelandSDKPrivatePCH.h"


// Sets default values for this component's properties
UVivelandBoomEffectDemo::UVivelandBoomEffectDemo()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    bWantsBeginPlay = true;
    PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UVivelandBoomEffectDemo::BeginPlay()
{
    Super::BeginPlay();
	if (UVivelandBoomEffect::init(6))
	    //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::White, "UVivelandBoomEffect init OK");
		UVivelandBoomEffect::play(1, 1);
	}

void UVivelandBoomEffectDemo::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);
	UVivelandBoomEffect::deinit();
}

// Called every frame
void UVivelandBoomEffectDemo::TickComponent(
        float DeltaTime,
        ELevelTick TickType,
        FActorComponentTickFunction* ThisTickFunction
)
{
    Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
}
