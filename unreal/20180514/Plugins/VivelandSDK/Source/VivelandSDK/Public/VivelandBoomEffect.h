// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Object.h"
#include "VivelandBoomEffect.generated.h"

/**
 * 
 */
UCLASS()

class VIVELANDSDK_API UVivelandBoomEffect : public UObject
{
	GENERATED_BODY()
public:
	static bool init(int id);
	static void deinit();
	static int getPorts();
	static void play(int index, int port = 1);
	static void setVol(int vol, int port = 1);
	static void stop(int port);
};