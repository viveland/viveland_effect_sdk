// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ModuleManager.h"

class FVivelandSDKModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	/**
	* Singleton-like access to this module's interface.  This is just for convenience!
	* Beware of calling this during the shutdown phase, though.  Your module might have been unloaded already.
	*
	* @return Returns singleton instance, loading the module on demand if needed
	*/
	static FVivelandSDKModule& Get()
	{
		return FModuleManager::LoadModuleChecked< FVivelandSDKModule >("VivelandSDK");
	}

	/**
	* Checks to see if this module is loaded and ready.  It is only valid to call Get()
	* if IsAvailable() returns true.
	*
	* @return True if the module is loaded and ready to use
	*/
	static bool IsAvailable()
	{
		auto is_available = FModuleManager::Get().IsModuleLoaded("VivelandSDK");
		if (is_available) {
			// Concatenate the plugins folder and the DLL file.
			auto file_path1 = FPaths::Combine(
				*FPaths::GameDir(),
				TEXT("Plugins/VivelandSDK/ThirdParty/VivelandEffect/Libraries/Win32"),
				TEXT("VivelandEffect.dll")
			);
			auto dll_handle1 = FPlatformProcess::GetDllHandle(*file_path1);
			if (dll_handle1 == nullptr) {
				file_path1 = FPaths::Combine(
					*FPaths::GameDir(),
					TEXT("Plugins/VivelandSDK/ThirdParty/VivelandEffect/Libraries/Win64"),
					TEXT("VivelandEffect.dll")
				);
				FPlatformProcess::GetDllHandle(*file_path1);
			}
		}
		return is_available;
	}
};