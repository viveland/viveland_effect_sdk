// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class VivelandSDK : ModuleRules
{
    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "../../ThirdParty/")); }
    }

    public VivelandSDK(ReadOnlyTargetRules Target) : base(Target)
    {
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				"VivelandSDK/Public"
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"VivelandSDK/Private",
                Path.Combine(ThirdPartyPath, "VivelandEffect", "includes"),
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

        LoadVivelandLibrary(Target);
    }

    public bool LoadVivelandLibrary(ReadOnlyTargetRules Target)
    {
        bool isLibrarySupported = false;

        if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
        {
            isLibrarySupported = true;
            string platformString = Target.Platform == UnrealTargetPlatform.Win64 ? "Win64" : "Win32";
            platformString = Path.Combine("Libraries", platformString);
            string LibrariesPath = Path.Combine(ThirdPartyPath, "VivelandEffect", platformString);

            PublicLibraryPaths.Add(LibrariesPath);
            PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "VivelandEffect.lib"));
            PublicDelayLoadDLLs.Add("VivelandEffect.dll");
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(LibrariesPath, "VivelandEffect.dll")));
        }

        if (isLibrarySupported)
        {
            // Include path
            PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "VivelandEffect", "includes"));
        }

        Definitions.Add(string.Format("WITH_VIVELANDSDK_BINDING={0}", isLibrarySupported ? 1 : 0));

        return isLibrarySupported;
    }
}
