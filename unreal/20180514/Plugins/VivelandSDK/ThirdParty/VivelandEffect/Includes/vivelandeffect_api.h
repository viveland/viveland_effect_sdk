#ifdef VIVELANDEFFECT_API_EXPORTS
#define VIVELANDEFFECT_API __declspec(dllexport) 
#else
#define VIVELANDEFFECT_API __declspec(dllimport) 
#endif


class IVivelandIBoomEffect
{
public:
	virtual bool init(int id) = 0;
	virtual void deinit() = 0;
	virtual int getPorts() = 0;
	virtual void play(int index, int port = 1) = 0;
	virtual void setVol(int vol, int port = 1) = 0;
	virtual void stop(int port) = 0;
};


VIVELANDEFFECT_API IVivelandIBoomEffect* VivelandBoomEffect();
VIVELANDEFFECT_API bool IVivelandIBoomEffect_init(int id);
VIVELANDEFFECT_API void IVivelandIBoomEffect_deinit();
VIVELANDEFFECT_API int	IVivelandIBoomEffect_getPorts();
VIVELANDEFFECT_API void IVivelandIBoomEffect_play(int index, int port =1);
VIVELANDEFFECT_API void IVivelandIBoomEffect_setVol(int vol, int port =1);
VIVELANDEFFECT_API void IVivelandIBoomEffect_stop(int port);

class IVivelandIWindEffect
{
public:
	virtual bool init(int id) = 0;
	virtual void deinit() = 0;
	virtual void setLevel(int level, int port = 1) = 0;
};

VIVELANDEFFECT_API IVivelandIWindEffect* VivelandWindEffect();
VIVELANDEFFECT_API bool IVivelandIWindEffect_init(int id);
VIVELANDEFFECT_API void IVivelandIWindEffect_deinit();
VIVELANDEFFECT_API void IVivelandIWindEffect_setLevel(int level, int port =1);


class IVivelandIKeyDevice
{
public:
	virtual bool init(int id, int port) = 0;
	virtual void deinit(int port) = 0;
	virtual void bind(BYTE vkey, bool lowactive, int port) = 0;
};

VIVELANDEFFECT_API IVivelandIKeyDevice* VivelandKeyDevice();
VIVELANDEFFECT_API bool IVivelandIKeyDevice_init(int id, int port = 1);
VIVELANDEFFECT_API void IVivelandIKeyDevice_deinit(int port = 1);
VIVELANDEFFECT_API void IVivelandIKeyDevice_bind(BYTE vkey, int port = 1, bool lowactive = true);


class IVivelandISwitchDevice
{
public:
	virtual bool init(int id, int port) = 0;
	virtual void deinit(int port) = 0;
	virtual void on(bool turn_on, int port) = 0;
};

VIVELANDEFFECT_API IVivelandISwitchDevice* VivelandSwitchDevice();
VIVELANDEFFECT_API bool IVivelandISwitchDevice_init(int id, int port = 1);
VIVELANDEFFECT_API void IVivelandISwitchDevice_deinit(int port = 1);
VIVELANDEFFECT_API void IVivelandISwitchDevice_on(bool turn_on = 1, int port = 1);

class IVivelandIServoDevice
{
public:
	virtual bool init(int id, int port) = 0;
	virtual void deinit(int port) = 0;
	virtual void setPos(int pos, int port = 1) = 0;
};

VIVELANDEFFECT_API IVivelandIServoDevice* VivelandServoDevice();
VIVELANDEFFECT_API bool IVivelandIServoDevice_init(int id, int port = 1);
VIVELANDEFFECT_API void IVivelandIServoDevice_deinit(int port);
VIVELANDEFFECT_API void IVivelandIServoDevice_setPos(int pos, int port = 1);
